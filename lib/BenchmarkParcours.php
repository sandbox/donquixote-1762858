<?php

class autoload_benchmark_BenchmarkParcours {

  protected $cases = array();

  function __construct() {
    $this->cases['findFile.before'] = new autoload_benchmark_BenchmarkCase_FindFile();
    $this->cases['loadClass'] = new autoload_benchmark_BenchmarkCase_LoadClass();
    $this->cases['findFile.after'] = new autoload_benchmark_BenchmarkCase_FindFile();
    $this->cases['loadClass.nested'] = new autoload_benchmark_BenchmarkCase_LoadClass('Nested\\SomeClass');
    $this->cases['loadClass.nonexisting'] = new autoload_benchmark_BenchmarkCase_LoadClass('NonExistingClass', TRUE);
  }

  function casesLabels() {
     $labels = array_keys($this->cases);
     return array_combine($labels, $labels);
  }

  function runAll($n_namespaces) {
    $loaders = $this->detectLoaderCandidates($n_namespaces);
    $n_loaders = count($loaders);
    foreach ($this->cases as $case) {
      $case->reset($n_loaders, $n_namespaces);
    }
    $matrix = array();
    $sort = array();
    foreach ($loaders as $loader_key => $loader) {
      foreach ($this->cases as $case_key => $case) {
        $t0 = microtime(TRUE);
        $case->run($loader, $loader_key);
        $t1 = microtime(TRUE);
        $dt = ($t1 - $t0) / $case->nOperations();
        $matrix[$loader_key][$case_key] = $dt;
      }
      $sort[$loader_key] = -$matrix[$loader_key]['loadClass'];
    }
    array_multisort($sort, $matrix);
    return $matrix;
  }

  protected function detectLoaderCandidates($n_namespaces) {
    $base = drupal_get_path('module', 'autoload_benchmark');
    $map = array();
    for ($i = 0; $i < $n_namespaces; ++$i) {
      $map["Drupal\\autoload_benchmark_$i"] = $base . "/modules/autoload_benchmark_$i/lib";
    }
    return module_invoke_all('autoload_benchmark_loader_candidates', $map);
  }
}
