<?php

class autoload_benchmark_BenchmarkCase_FindFile {

  protected $nNamespaces = 300;
  protected $nInner = 3;

  function reset($n_loaders, $n_namespaces) {
    $this->nNamespaces = $n_namespaces;
  }

  function run($loader) {
    for ($i = 0; $i < $this->nNamespaces; ++$i) {
      $class = "Drupal\\autoload_benchmark_$i\\SomeClass";
      for ($j = 0; $j < $this->nInner; ++$j) {
        $loader->findFile($class);
      }
    }
  }

  function nOperations() {
    return $this->nInner * $this->nNamespaces;
  }
}
