<?php

class autoload_benchmark_BenchmarkCase_LoadClass {

  protected $append;
  protected $failed;
  protected $nLoaders;
  protected $classes = array();
  protected $index = 0;
  protected $nOperations;

  function __construct($append = 'SomeClass', $failed = FALSE) {
    $this->append = $append;
    $this->failed = $failed;
  }

  function reset($n_loaders, $n_namespaces) {
    // We need to evenly distribute the indexes, because classloader needs longer
    // for higher indexes.
    $this->nLoaders = $n_loaders;
    $this->nOperations = floor($n_namespaces / $n_loaders);
    $n = $this->nOperations * $n_loaders;
    for ($iStart = 0; $iStart < $n_loaders; ++$iStart) {
      $classes = array();
      for ($i = $iStart; $i < $n; $i += $n_loaders) {
        $classes[] = "Drupal\\autoload_benchmark_$i\\" . $this->append;
      }
      $this->classes[] = $classes;
    }
  }

  function run($loader, $loader_key) {
    foreach ($this->classes[$this->index] as $class) {
      $before = class_exists($class, FALSE);
      if ($before) {
        throw new Exception("Class $class already loaded.");
      }
      $loader->loadClass($class);
      $after = class_exists($class, FALSE);
      if (!$after && !$this->failed) {
        throw new Exception("Failed to load $class with $loader_key");
      }
    }
    ++$this->index;
  }

  function nOperations() {
    return $this->nOperations;
  }
}
