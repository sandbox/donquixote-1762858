<?php

/**
 * Helper function to create a directory if it doesn't exist yet.
 */
function mkdir_if_not_exists($dir) {
  if (!is_dir($dir)) {
    mkdir($dir);
  }
}

// Create a subdirectory for the submodules.
$base = dirname(__FILE__);
mkdir_if_not_exists($base . '/modules');

// Create 300 submodules.
// Note: We don't create any *.info or *.module files, so these are not going to
// be real Drupal modules. Instead, we will register the namespaces manually.
for ($i = 0; $i < 300; ++$i) {

  // Create the module folders.
  $module = 'autoload_benchmark_' . $i;
  $module_dir = "$base/modules/$module";
  $namespace_dir = "$module_dir/lib/Drupal/$module";
  mkdir_if_not_exists($module_dir);
  mkdir_if_not_exists($module_dir . '/lib');
  mkdir_if_not_exists($module_dir . '/lib/Drupal');
  mkdir_if_not_exists($namespace_dir);

  // Create the SomeClass class file.
  $php = <<<EOT
<?php
namespace Drupal\\$module;
class SomeClass {}
EOT;
  file_put_contents($namespace_dir . '/SomeClass.php', $php);

  // Create the Nested\SomeClass class file.
  $php = <<<EOT
<?php
namespace Drupal\\$module\\Nested;
class SomeClass {}
EOT;
  mkdir_if_not_exists($namespace_dir . '/Nested');
  file_put_contents($namespace_dir . '/Nested/SomeClass.php', $php);
}
