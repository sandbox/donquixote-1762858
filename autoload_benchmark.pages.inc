<?php

/**
 * Page callback
 */
function _autoload_benchmark_page() {

  $html = '';

  if (!module_exists('xautoload') || !module_exists('classloader')) {
    $html .= '<p>Ideally you install both xautoload and classloader for this benchmark.</p>';
  }

  $html .= '<h2>Registered class loaders</h2>';

  $overview = array();
  $loaders = array();
  foreach (spl_autoload_functions() as $f) {
    if (is_array($f) && is_object($f[0])) {
      list($obj, $method) = $f;
      $class = get_class($obj);
      $str = $class . '->' . $method;
      if (preg_match('/^xautoload_ClassLoader/', $class)) {
        $loaders['xautoload'] = $f;
      }
      elseif (preg_match('#^Symfony\\\\Component\\\\ClassLoader\\\\#', $class)) {
        $loaders['classloader'] = $f;
      }
    }
    else {
      $str = $f;
    }
    $overview[] = $str;
  }
  $html .= '<pre>' . print_r($overview, TRUE) . '</pre>';


  $parcours = new autoload_benchmark_BenchmarkParcours();

  $n = !empty($_GET['n']) ? (int)$_GET['n'] : 300;
  if (!is_numeric($n) || $n < 100 || $n > 300) {
    $n = 300;
  }
  $html .= "<h2>Benchmark results: $n</h2>";
  $html .= '<p>(avg microseconds per single operation)</p>';
  $html .= "<p>We register $n test namespaces into each loader, and see how well they perform.</p>";
  $matrix = $parcours->runAll($n);
  $html .= _autoload_benchmark_matrix_html($parcours->casesLabels(), $matrix);

  return $html;
}

/**
 * Render a table showing benchmark results.
 */
function _autoload_benchmark_matrix_html($cases, $matrix, $exp = FALSE) {

  if (empty($matrix)) {
    return;
  }

  $html = '';
  foreach ($matrix as $loader_key => $loader_results) {
    $row_html = '<td>' . $loader_key . '</td>';
    foreach ($cases as $case_key => $case_label) {
      $seconds = $loader_results[$case_key];
      $micros = $seconds * 1000 * 1000;
      if (0 === $micros) {
        $str = '';
      }
      elseif ($exp) {
        $str = number_format(log($micros, 2), 3);
      }
      else {
        $str = number_format($micros, 3);
      $row_html .= '<td style="text-align:right;"><pre>' . $str . '</pre></td>';
      }
    }
    $html .= '<tr>' . $row_html . '</tr>';
  }

  $head = '<td></td>';
  foreach ($cases as $case_key => $case_label) {
    $head .= '<td>' . $case_label . '</td>';
  }

  return '<table style="width:auto;"><thead><tr>' . $head . '</tr></thead><tbody>' . $html . '</tbody></table>';
}
